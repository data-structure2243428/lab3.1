import java.util.Scanner;

public class SqrtLab {
    public static int binarySearch(int x) {
        int left = 1;
        int right = x;
        
        while (left <= right) {
            int mid = left + (right - left) / 2;
            int sqrt = x/mid;
            if (mid == sqrt ) {
                return mid; 
            } else if (mid < sqrt) {
                left = mid + 1; 
            } else {
                right = mid - 1; 
            }
        }
        return right; 
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        System.out.println(binarySearch(x));
    }
}
