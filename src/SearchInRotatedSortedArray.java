import java.util.Scanner;

public class SearchInRotatedSortedArray{
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int x;
        System.out.print("Input: x = ");
        x = kb.nextInt();
        double a = Math.sqrt(x);
        System.out.println("Output: "+(int)Math.floor(a));
        System.out.println("Explanation: The square root of "+x+" is "+(int)Math.floor(a)+", so we return "+(int)Math.floor(a));
    }

}
